# encore-un-my-cool-anti-ed-bot
> 

## Installation

```sh
$ git clone https://gitlab.com/encore-un/encore-un-my-cool-anti-ed-bot
$ cd encore-un-my-cool-anti-ed-bot
$ npm i
```

## Usage

```sh
$ mv config.js.example config.js
```

Edit config.js and put your token and bot name as follow:


```js
module.exports = {

  // Your bot name. Typically, this is your bot's username without the discriminator.
  // i.e: if your bot's username is MemeBot#0420, then this option would be MemeBot.
  name: process.env.BOT_NAME,

  // The bot's command prefix.
  prefix: process.env.BOT_PREFIX,

  // Your bot's user token. If you don't know what that is, go here:
  // https://discordapp.com/developers/applications/me
  // Then create a new application and grab your token.
  token: process.env.BOT_TOKEN
};
```

```sh
$ npm run bot
```
## License

BSD-3-Clause © [Mania]()


[npm-image]: https://badge.fury.io/js/encore-un-my-cool-anti-ed-bot.svg
[npm-url]: https://npmjs.org/package/encore-un-my-cool-anti-ed-bot
[daviddm-image]: https://david-dm.org//encore-un-my-cool-anti-ed-bot.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//encore-un-my-cool-anti-ed-bot
[coveralls-image]: https://coveralls.io/repos//encore-un-my-cool-anti-ed-bot/badge.svg
[coveralls-url]: https://coveralls.io/r//encore-un-my-cool-anti-ed-bot
