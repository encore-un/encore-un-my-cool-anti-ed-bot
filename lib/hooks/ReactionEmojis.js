const Hook = require('../modules/hook');

const reactions = [
  {
    regExp: /.*(lj|jaja).*/i,
    emoji: 'lj'
  },
  {
    regExp: /.*(^|\s)(benji|cool)(\s|$).*/i,
    emoji: 'benji'
  },
  {
    regExp: /.*(^|\s)(simon|snzy|chat)(\s|$).*/i,
    emoji: 'snzy'
  },
  {
    regExp: /.*(^|\s)(raytho|thomas)(\s|$).*/i,
    emoji: 'maot'
  },
  {
    regExp: /.*(^|\s)mania(\s|$).*/i,
    emoji: 'mania'
  },
  {
    regExp: /.*(^|\s)ed(\s|$).*/i,
    emoji: 'ed'
  },
  {
    regExp: /.*(^|\s)(ah|ha)(!|\s|$).*/i,
    emoji: 'ha'
  },
  {
    regExp: /.*:ha:.*/i,
    emoji: 'ha'
  },
  {
    regExp: /.*(^|\s)arnaud?(\s|$).*/i,
    emoji: 'kreygasm'
  },
  {
    regExp: /.*(^|\s)(travailler|bosser)(\s|$).*/i,
    emoji: 'bobross'
  },
  {
    regExp: /.*.*(^|\s)auchan(\s|$).*.*/i,
    emoji: 'masterrace'
  },
  {
    regExp: /.*(^|\s)aled(\s|$).*/i,
    emoji: 'aled'
  },
  {
    regExp: /.*(^|\s)(pug|ronpiche|orthographe)(\s|$).*/i,
    emoji: 'ronpiche'
  },
  {
    regExp: /.*(^|\s)(c'est qui|c'est quoi)(\s|$).*/i,
    emoji: ':thinking:'
  }
];

module.exports = new Hook.MessageHook({
  name: `Emojis`,
  desc: `Réagis à des message par des emojis`,

  onMessage: params => {
    let {msg} = params;

    reactions.forEach(reaction => {
      if (msg.content.search(reaction.regExp) !== -1) {
        let emoji = msg.channel.guild.emojis.find(emoji => emoji.name === reaction.emoji);

        emoji = emoji ? emoji : reaction.emoji;

        msg.react(emoji)
          .then(() => console.log(`reacted emoji ${reaction.emoji}`))
          .catch(err => console.error(err.message));
      }
    });
  },

  userFilter: params => {
    let {bot, member} = params;
    return bot.user.id !== member.user.id;
  }
});
