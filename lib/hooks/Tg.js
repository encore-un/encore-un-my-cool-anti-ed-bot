const Hook = require('../modules/hook');
const mentionsService = require('../services/mention');
const winston = require('winston');

const regExp = /tg(\s.*?)(\s(\d+))?/;
const limitRegExp = /\s(\d+)/;
const defaultLimit = 10;

module.exports = new Hook.RegExpMessageHook({
  name: `tg`,
  desc: `tg @mentions [n] | Parcourt les [n] derniers messages et supprimes ceux qui correspondent à un utilisateur mentionné (fonctionne avec les groupes)`,
  start: true,
  prefix: true,
  regExp: regExp,

  onMessage: params => {
    let {msg} = params;
    let limit = defaultLimit;

    // Try to get user defined limit
    let limitMatches = msg.content.match(limitRegExp);

    if (limitMatches) {
      limit = limitMatches[1];
    }

    let targetedUsersId = mentionsService.getUsersTargetedByMentions(msg.client, msg.mentions).map(user => user.id);

    winston.log('info', 'ids', {ids: targetedUsersId});

    msg.channel.fetchMessages({limit: limit, before: msg.id})
      .then(messages => messages
        .filter(message => targetedUsersId.indexOf(message.author.id) !== -1)
        .forEach(message =>
          message.delete()
            .then(m => winston.log('info', `Deleted message from ${m.author.username} by ${msg.author.username}`))
            .catch(() => {
              return winston.log('error', 'Error while deleting messages');
            })
        ))
      .catch(() => {
        return winston.log('error', 'Error while getting messages');
      });
  },

  userFilter: params => (params.member.hasPermission('MANAGE_MESSAGES') || params.member.user.username === 'Raytho') // Chut
});
