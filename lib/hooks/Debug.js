const Hook = require('../modules/hook');

module.exports = new Hook.MessageHook({
  name: `debug`,
  desc: ``,

  onMessage: () => {
    // let {bot, msg} = params;
  },

  userFilter: params => {
    let {bot, member} = params;
    return bot.user.id !== member.user.id;
  }
});
