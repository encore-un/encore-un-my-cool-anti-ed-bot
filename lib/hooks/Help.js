const Hook = require('../modules/hook');
const os = require('os');
const cfg = require('../../config.js');
const RegExpMessageHook = require('../modules/hook/index.js').RegExpMessageHook;

const regExp = /aled/;

module.exports = new Hook.RegExpMessageHook({
  name: `aled`,
  desc: `A.L.ED: Affiche L'ED`,
  start: true,
  prefix: true,
  regExp: regExp,
  ignoreBot: true,

  onMessage: params => {
    let {app, msg} = params;

    let text = ``;

    app.hooks
      .filter(hook => hook instanceof RegExpMessageHook && hook.options.userFilter(params))
      .forEach(hook => {
        text += `\`${hook.options.prefix ? cfg.prefix : ''}${hook.options.name}\`: ${hook.options.desc}${os.EOL}`;
      })
    ;

    msg.channel.sendMessage(text);
  }
});
