const MessageHook = require('./MessageHook');
const cfg = require('../../../config');
const defaults = require('lodash.defaults');

/**
 * @class MessageHook
 * MessageHook based on a regular expression
 * @extends MessageHook
 */
class RegExpMessageHook extends MessageHook {

  /**
   * Create a message hook based on regular expression
   *
   * @param {Object} options
   * @param {string} options.name - Hook name
   * @param {string} [options.desc] - Hook description
   * @param {onMessage} options.onMessage - Called on matching message
   * @param {userFilter} [options.userFilter=() => true] - User filter
   * @param {boolean} [options.ignoreBot=true] - If set true, won't hook on bot messages
   * @param {boolean} [options.caseInsensitive=false] - If set true, will ignore case for matching
   * @param {boolean} [options.prefix=true] - If set true, add the environment prefix to the RegExp
   * @param {boolean} [options.start=true] - If set true, verify that it's match at the start of the message
   * @param {string} options.regexp - RegExp to match
   */
  constructor(options) {
    defaults(options, {
      prefix: true,
      start: true,
      caseInsensitive: false
    });

    // Override onMessage
    let onMessage = options.onMessage;

    options.onMessage = function (params) {
      let content = params.msg.content;

      let start = new RegExp(`${options.start ? '^' : ''}`);
      let prefix = new RegExp(`${options.prefix ? cfg.prefix : ''}`);
      let flags = `${options.caseInsensitive ? 'i' : ''}`;

      let regExp = new RegExp(start.source + prefix.source + options.regExp.source, flags);

      if (content.search(regExp) !== -1) {
        params.matches = content.match(regExp);
        onMessage(params);
      }
    };

    super(options);
  }
}

module.exports = RegExpMessageHook;
