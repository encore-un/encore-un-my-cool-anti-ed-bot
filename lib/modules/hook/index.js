const HookApp = require('./HookApp');
const Hook = require('./Hook');
const MessageHook = require('./MessageHook');
const RegExpMessageHook = require('./RegExpMessageHook');

module.exports = {
  HookApp: HookApp,
  Hook: Hook,
  MessageHook: MessageHook,
  RegExpMessageHook: RegExpMessageHook
};
