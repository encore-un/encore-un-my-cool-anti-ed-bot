const MessageHook = require('./MessageHook');

/**
 * @callback onMessage
 *
 * @param {Object} params
 * @param {HookApp} params.app - Hook application
 * @param {Message} params.msg - Discord.js [Message]{@link https://discord.js.org/#!/docs/tag/master/class/Message} object
 * @param {Client} params.bot - Discord.js [Client]{@link https://discord.js.org/#!/docs/tag/master/class/Client} object
 */

/**
 * Simple hook application,
 */
class HookApp {
  /**
   * Create a hook app
   * @param {Object} options
   * @param {Client} options.bot
   *
   */
  constructor(options) {
    this.bot = options.bot;

    this.bot.on('message', msg => {
      this.onMessage({
        msg: msg
      });
    });

    this.hookArray = [];
  }

  /**
   * Add an hook to the application
   * @param {Hook} hook
   */
  addHook(hook) {
    this.hookArray.push(hook);
  }

  /**
   * Retrieve all hooks
   * @returns {Hook[]} hook
   */
  get hooks() {
    return this.hookArray;
  }

  /**
   * Call on message event emitted
   * @param params
   * @param {Message} params.msg
   */
  onMessage(params) {
    let {msg} = params;
    let self = this;

    self = this;

    this.hookArray
      .filter(hook => hook instanceof MessageHook)
      .forEach(function (hook) {
        try {
          hook.onMessage({
            bot: self.bot,
            msg: msg,
            app: self
          });
        } catch (e) {
          console.error(`Error in hook "${hook.options.name}": ${e.message}`);
        }
      })
    ;
  }
}

module.exports = HookApp;
