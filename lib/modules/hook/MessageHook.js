'use strict';
const Hook = require('./Hook');
const defaults = require('lodash.defaults');

/**
 * @callback userFilter
 *
 * @param {GuildMember} member - Discord.js [GuildMember]{@link https://discord.js.org/#!/docs/tag/master/class/GuildMember} object
 */

/**
 * Hook called on a message send in text channel
 */
class MessageHook extends Hook {
  /**
   * Create a MessageHook
   *
   * @param {Object} options
   * @param {string} options.name - Hook name
   * @param {string} [options.desc] - Hook description
   * @param {function} options.onMessage - Called on matching message
   * @param {onMessage} [options.userFilter=() => true] - User filter
   * @param [options.ignoreBot=true] - If set true, won't hook on bot messages
   *
   */
  constructor(options) {
    defaults(options, {
      desc: '',
      ignoreBot: true,
      userFilter: () => true
    });

    // Override onMessage
    let onMessage = options.onMessage;

    options.onMessage = params => {
      let {msg} = params;

      let userFilter = options.userFilter;
      let author = msg.author;

      if (author.bot && options.ignoreBot) {
        return;
      }

      let members = msg.channel.guild.members;
      params.member = members.find(member => member.id === author.id);

      if (userFilter(params)) {
        return onMessage(params);
      }
    };

    super(options);

    this.onMessage = options.onMessage;
  }
}

module.exports = MessageHook;
