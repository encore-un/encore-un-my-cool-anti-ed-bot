/* eslint-disable */
/**
 * Base Hook class
 */
class   Hook {
  /**
   * Hook constructor
   *
   * @param {Object} options - HookParams object
   * @param {string} options.name - Hook name
   * @param {string} [options.desc] - Hook description
   */
  constructor(options) {
    this.options = options;
  }
}

module.exports = Hook;
